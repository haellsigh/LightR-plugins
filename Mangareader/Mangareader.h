#ifndef MANGAREADER_H
#define MANGAREADER_H

#include <QtPlugin>
#include <WebsiteInterface.h>

#include <QDownloadManager/Core.h>
#include <QWebFrame>
#include <QWebPage>
#include <QWebElement>
#include <QWebElementCollection>

class Mangareader : public QObject, public WebsiteInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.LightR.WebsiteInterface/1.0" FILE "Mangareader.json")
    Q_INTERFACES(WebsiteInterface)

public:
    Mangareader();
    ~Mangareader();

    QList<Manga::baseManga> getAllMangas() const Q_DECL_OVERRIDE;
    Manga::Manga getManga(QString name) const Q_DECL_OVERRIDE;

public slots:
    void on_downloadFinished(QString download);

signals:

private:
    void setMangaData();

    QList<Manga::Manga> mAllMangas;

    QDownloadManager *mDownloadManager;

    QWebPage mWebPage;
    QWebFrame *mWebFrame;
};

#endif // MANGAREADER_H
