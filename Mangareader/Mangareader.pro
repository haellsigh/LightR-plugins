#-------------------------------------------------
#
# Project created by QtCreator 2015-04-01T21:18:14
#
#-------------------------------------------------

include(../../LightR/QDownloadManager/QDownloadManager.pri)

CONFIG +=       plugin
QT +=           webkitwidgets network
TARGET =        Mangareader
TEMPLATE =      lib

INCLUDEPATH +=  ../../LightR

SOURCES +=      Mangareader.cpp
HEADERS +=      Mangareader.h

DESTDIR =       ../../build-LightR-Desktop_Qt_5_4_1_MinGW_32bit-Debug/plugins

OTHER_FILES +=  Mangareader.json
