#include "Mangareader.h"

#include <QDebug>

Mangareader::Mangareader()
{
    mWebFrame = mWebPage.mainFrame();
    mWebFrame->setParent(this);

    mDownloadManager = new QDownloadManager(this);
    mDownloadManager->addDownload("allMangas", "http://www.mangareader.net/alphabetical");
    connect(mDownloadManager, SIGNAL(downloadFinished(QString)), SLOT(on_downloadFinished(QString)));
    qDebug() << "PLUGINS/Mangareader: Initialized";
}

Mangareader::~Mangareader()
{
}

QList<Manga::baseManga> Mangareader::getAllMangas() const
{
    QList<Manga::baseManga> list;
    return list;
}

Manga::Manga Mangareader::getManga(QString name) const
{
    Manga::Manga manga;
    return manga;
}

void Mangareader::on_downloadFinished(QString download)
{
    qDebug() << "PLUGINS/Mangareader: request finished";
    if(download == "allMangas")
    {
        mWebFrame->setContent(mDownloadManager->retrieveDownloadedData(download));
        setMangaData();
    }
}

void Mangareader::setMangaData()
{
    QWebElement webElement = mWebFrame->documentElement();

    QWebElementCollection mangaList;
    mangaList = webElement.findAll(".series_alpha");
    qDebug() <<"BEFORE LOOP !!";
    if(mangaList.count() == 0)
        qDebug() << "List empty!";
    foreach (QWebElement currentManga, webElement.findAll(".series_alpha")) {
        qDebug() << currentManga.toPlainText();
    }

}
